var pkgJson = require('./package.json');
var version = pkgJson.version;

//util
var del = require('del');
var merge = require('merge2');
var argv = require('yargs').argv;

// gulp module
var gulp = require('gulp');
var gutil = require('gulp-util');
var gprint = require('gulp-print');
var grunSequence = require('run-sequence').use(gulp);
var gchanged = require('gulp-changed');
var gheader = require('gulp-header');


//project compile
var gless = require('gulp-less');
var gtypescript = require('gulp-typescript');


var buildPath = 'build';

//pathes
var buildLessPath = ['main/**/*.less'];
var buildResPath = ['main/**/*', '!main/**/*.tsx', '!main/**/*.ts','!main/**/*.less'
        ,'node_modules/@atticcat/react-webkit/build/dist/*'];
var buildTypescriptPath = ['main/**/*.ts', 'main/**/*.tsx'];

//by --prompt
var prompt = argv.prompt;

gulp.task('default', function () {
    var usage = [
        "Usage:",
        "\tgulp clean \t\tTo clean entire build folder",
        "\tgulp build \t\tTo build the project",
        "\tgulp watch \t\tTo build then watch the project",
        "\tgulp server \t\tTo start test server",
        "\nParameters:",
        "\t--prompt",
    ].join('\n');
    console.log(usage);
});

gulp.task('clean', function () {
    return del([buildPath + '/**']).then(function (paths) {
        if (prompt) {
            console.log('Deleted files and folders:\n', paths.join('\n'));
        }
    });
});

gulp.task('build', function () {
    grunSequence('buildTypescript', 'buildLess', 'buildRes');
});
gulp.task('buildLess', function () {
    var dist = buildPath;
    return gulp.src(buildLessPath)
        .pipe(gprint(function (path) {
            return prompt ? "Compiling less " + path : null;
        }))
        .pipe(gless())
        .pipe(licenseHeader())
        .pipe(gulp.dest(dist))
        .pipe(gprint(function (path) {
            return prompt ? "Write " + path : null;
        }))
});
gulp.task('buildRes', function () {
    var dist = buildPath;
    var result = gulp.src(buildResPath)
        .pipe(gchanged(dist));
    return result.pipe(gulp.dest(dist))
        .pipe(gprint(function (path) {
            return prompt ? "Write " + path : null;
        }));
});

gulp.task('buildTypescript', function () {
    var dist = buildPath;
    var tsConfig = {
        //gulp-typescript config
        noExternalResolve: false,
        typescript: require('typescript')
    }
    var tsProject = gtypescript.createProject('./tsconfig.json', tsConfig);

    var tsResult = gulp.src(buildTypescriptPath)
        .pipe(gchanged(dist, { extension: '.js' }))
        .pipe(gprint(function (path) {
            return prompt ? "Compiling typescript " + path : null;
        }))
        .pipe(gtypescript(tsProject))

    //merge for gulp task waiting on both
    var result = tsResult.js
        .pipe(licenseHeader())
        .pipe(gulp.dest(dist))
        .pipe(gprint(function (path) {
            return prompt ? "Write " + path : null;
        }));
    return result;
});


function licenseHeader() {
    return gheader([
        '/**',
        ' * <%= pkg._ext.name %> - v<%= pkg.version %>',
        ' * <%= pkg.description %>',
        ' * ',
        ' * Copyright 2016 - present, <%= pkg.author %>, All rights reserved.',
        ' * ',
        ' * Released under <%= pkg.license %> license',
        ' */',
        ''
    ].join('\n'), { pkg: pkgJson });
}

gulp.task('watch', function () {
    gulp.watch(buildLessPath, ['buildLess']);
    gulp.watch(buildResPath, ['buildRes']);
    gulp.watch(buildTypescriptPath, ['buildTypescript']);
})


gulp.task('server', function () {
    var express = require('express');
    var app = express();
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var serveIndex = require('serve-index');

    //cookie
    app.use(cookieParser());

    //content parser
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/ping', function (req, res) {
        res.end('pong');
    });
    
    //file accessing
    app.use('/', express.static('build'));
    //directory listing
    app.use('/', serveIndex('build', { icons: true, view: 'details' }))
    

    var port = 8080;
    var server = app.listen(port, function () {
        var host = server.address().address
        var port = server.address().port
        console.log('WebApp started, listening at http://%s:%s', host, port);
    });
});