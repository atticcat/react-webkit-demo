/// <reference path="../node_modules/@atticcat/react-webkit/build/3rd-definition/react.d.ts" />
/// <reference path="../node_modules/@atticcat/react-webkit/build/3rd-definition/react-dom.d.ts" />
/// <reference path="../node_modules/@atticcat/react-webkit/build/main/react-webkit.d.ts" />
import React = require('react');
import ReactDOM = require('react-dom');

import wk = require('@atticcat/react-webkit');
let w = wk.Widget;
let i = wk.Input;
let l = wk.Layout;
let ls = wk.List;

export class App extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
    }
    render() {

        return (
            <l.Vlayout vflex={1} hflex={1} style={{ padding: '2px' }}>
                <l.Hlayout tooltip='Tooltip here'>
                    <i.Checkbox label='A Checkbox'/>
                    <i.Radiobox label='A Radio'/>
                </l.Hlayout>
                <ls.List vflex={1} hflex={1} >
                    <w.Anchor>List item 1</w.Anchor>
                    <w.Anchor>List item 2</w.Anchor>
                    <w.Anchor>List item 3</w.Anchor>
                </ls.List>

            </l.Vlayout>
        )
    }
}

export function render(dom: Element) {
    ReactDOM.render(<App/>,dom);
}