var lib = "jslib";
requirejs.config({
    paths: {
        "react": lib + "/react.min",
        "react-dom": lib + "/react-dom.min",
        "jquery": lib + "/jquery.min",
        "@atticcat/react-webkit": "react-webkit.min" 
    },
});